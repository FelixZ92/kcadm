ARG KEYCLOAK_VERSION=11.0.2
FROM jboss/keycloak:${KEYCLOAK_VERSION}

FROM openjdk:8-alpine

ENV KEYCLOAK_ENDPOINT http://keycloak:8080/auth
ENV KEYCLOAK_USER admin
ENV KEYCLOAK_PASS admin
ENV KEYCLOAK_REALM master

ARG KEYCLOAK_VERSION

RUN echo ${KEYCLOAK_VERSION}

WORKDIR /keycloak
COPY --from=0 /opt/jboss/keycloak/bin/kcadm.sh .
COPY --from=0 /opt/jboss/keycloak/bin/client/keycloak-admin-cli-${KEYCLOAK_VERSION}.jar ./client/

RUN ln -s /keycloak/kcadm.sh /usr/local/bin/kcadm
RUN apk --no-cache add ca-certificates curl gettext jq
RUN wget -O /usr/local/bin/yq "https://github.com/mikefarah/yq/releases/download/2.4.0/yq_linux_amd64"
RUN chmod u+x /usr/local/bin/yq

RUN mkdir -p /.keycloak && chown -R nobody:nobody /keycloak /.keycloak
USER nobody
